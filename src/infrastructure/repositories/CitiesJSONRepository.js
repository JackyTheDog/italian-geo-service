
const citiesJSON = require('../../../data/IT/cities.json');

const City = require('../../domain/City');
const CityNotFoundException = require('../../domain/exceptions/CityNotFoundException');
const LocationNameMatcher = require('../matchers/LocationNameMatcher');

class CitiesJSONRepository {

  constructor(aLocationNameMatcher) {
    this.locationNameMatcher = aLocationNameMatcher;
  }

  findByIstatCode(aIstatCode) {
    let cityJSON = citiesJSON.find(
      filterCityJSONByIstatCode
        .bind(null, aIstatCode)
    );

    if (!doesCityExist(cityJSON)) {
      throw new CityNotFoundException(`No city with istat code "${aIstatCode}" was found`);
    }

    return adaptCityJSONToDomain(cityJSON);
  }

  findAllByProvinceId(aProvinceId) {
    let listOfCitiesForProvinceId = citiesJSON
      .filter(
        filterCityJSONByProvinceId
          .bind(null, aProvinceId)
      )
      .map(adaptCityJSONToDomain);

    if (!listOfCitiesForProvinceId.length) {
      throw new CityNotFoundException(`No city for province code "${aProvinceId}" was found`);
    }

    return listOfCitiesForProvinceId;
  }

  findAllByMatchingName(aNameNeedle) {
    let listOfCitiesWithAMatchingName = citiesJSON
      .filter(aCityJSON => {
        return this.locationNameMatcher
          .findMatch(aCityJSON.name, aNameNeedle)
          .isMatch;
      })
      .map(adaptCityJSONToDomain);

    if (!listOfCitiesWithAMatchingName.length) {
      throw new CityNotFoundException(`No city matching name "${aNameNeedle}" was found`);
    }

    return listOfCitiesWithAMatchingName;
  }
}

function filterCityJSONByIstatCode(aIstatCode, aCityJSON) {
  return aCityJSON.istatCode === aIstatCode;
}

function doesCityExist(aCityJSON) {
  return !!aCityJSON;
}

function adaptCityJSONToDomain(aCityJSON) {
  return new City(aCityJSON);
}

function filterCityJSONByProvinceId(aProvinceId, aCityJSON) {
  return aCityJSON.provinceId === aProvinceId;
}

module.exports = CitiesJSONRepository;

