
const RegionsJSONRepository = require('../RegionsJSONRepository');

const Region = require('../../../domain/Region');
const RegionNotFoundException = require('../../../domain/exceptions/RegionNotFoundException');
const LocationNameMatcher = require('../../matchers/LocationNameMatcher');

describe('RegionsJSONRepository', () => {
  let repo;
  let aLocationNameMatcher;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();
    repo = new RegionsJSONRepository(aLocationNameMatcher);
  });

  describe('when required to find a region by id', () => {
    describe('and the region with that id does exist', () => {
      it('should return the region model', () => {
        let aId = '01';
        let region = repo.findById(aId);

        expect(region).toBeInstanceOf(Region);
        expect(region.id()).toBe(aId);
      });
    });

    describe('and the region with that id does NOT exist', () => {
      it('should throw a RegionNotFoundException exception', () => {
        expect(() => { repo.findById('BOH'); })
          .toThrow(RegionNotFoundException);
      });
    });
  });

  describe('when required to find all regions matching a partial name', () => {
    describe('and some region matching that name exists', () => {
      it('should return the list of matching regions', () => {
        let aNameNeedle = 'emi';
        let regions = repo.findAllByMatchingName(aNameNeedle);

        expect(regions.length).toBe(1);
        expect(regions[0].name()).toBe('Emilia-Romagna');
      });
    });

    describe('and NO region matching that name exists', () => {
      it('should throw a RegionNotFoundException exception', () => {
        expect(() => { repo.findAllByMatchingName('BOH'); })
          .toThrow(RegionNotFoundException);
      });
    });
  });
});

