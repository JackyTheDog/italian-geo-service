
const regionsJSON = require('../../../data/IT/regions.json');

const Region = require('../../domain/Region');
const RegionNotFoundException = require('../../domain/exceptions/RegionNotFoundException');
const LocationNameMatcher = require('../matchers/LocationNameMatcher');

class RegionsJSONRepository {

  constructor(aLocationNameMatcher) {
    this.locationNameMatcher = aLocationNameMatcher;
  }

  findById(aId) {
    let regionJSON = regionsJSON.find(
      filterRegionJSONById
        .bind(null, aId)
    );

    if (!doesRegionExist(regionJSON)) {
      throw new RegionNotFoundException(`No region with id "${aId}" was found`);
    }

    return adaptRegionJSONToDomain(regionJSON);
  }

  findAllByMatchingName(aNameNeedle) {
    let listOfRegionsWithAMatchingName = regionsJSON
      .filter(aRegionJSON => {
        return this.locationNameMatcher
          .findMatch(aRegionJSON.name, aNameNeedle)
          .isMatch;
      })
      .map(adaptRegionJSONToDomain);

    if (!listOfRegionsWithAMatchingName.length) {
      throw new RegionNotFoundException(`No region matching name "${aNameNeedle}" was found`);
    }

    return listOfRegionsWithAMatchingName;
  }
}

function filterRegionJSONById(aId, aRegionJSON) {
  return aRegionJSON.id === aId;
}

function doesRegionExist(aRegionJSON) {
  return !!aRegionJSON;
}

function adaptRegionJSONToDomain(aRegionJSON) {
  return new Region(aRegionJSON);
}

module.exports = RegionsJSONRepository;

