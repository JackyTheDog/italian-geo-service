
class FindLocationsByNameController {

  constructor(
    aCitiesJSONRepo,
    aProvincesJSONRepo,
    aRegionsJSONRepo
  ) {
    this.citiesJSONRepo = aCitiesJSONRepo;
    this.provincesJSONRepo = aProvincesJSONRepo;
    this.regionsJSONRepo = aRegionsJSONRepo;
  }

  findByMatchingName(
    aNameNeedle,
    aListOfLocationTypes = null
  ) {
    return []
      .concat(
        findCitiesByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle)
      )
      .concat(
        findProvincesByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle)
      )
      .concat(
        findRegionsByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle)
      );
  }

  findCitiesByMatchingName(aNameNeedle) {
    return executeOrReturnEmptyOnError(
      this.citiesJSONRepo.findAllByMatchingName
        .bind(this.citiesJSONRepo, aNameNeedle)
    );
  }

  findProvincesByMatchingName(aNameNeedle) {
    return executeOrReturnEmptyOnError(
      this.provincesJSONRepo.findAllByMatchingName
        .bind(this.provincesJSONRepo, aNameNeedle)
    );
  }

  findRegionsByMatchingName(aNameNeedle) {
    return executeOrReturnEmptyOnError(
      this.regionsJSONRepo.findAllByMatchingName
        .bind(this.regionsJSONRepo, aNameNeedle)
    );
  }
}

function findCitiesByMatchingNameIfRequested(aListOfLocationTypes, aNameNeedle) {
  return executeIfNeededOrReturnEmpty(
    this.findCitiesByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'CITIES')
  );
}

function findProvincesByMatchingNameIfRequested(aListOfLocationTypes, aNameNeedle) {
  return executeIfNeededOrReturnEmpty(
    this.findProvincesByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'PROVINCES')
  );
}

function findRegionsByMatchingNameIfRequested(aListOfLocationTypes, aNameNeedle) {
  return executeIfNeededOrReturnEmpty(
    this.findRegionsByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'REGIONS')
  );
}

function isLocationTypeRequested(aListOfLocationTypes, aLocationType) {
  return (
    !aListOfLocationTypes ||
    aListOfLocationTypes.indexOf(aLocationType) !== -1
  );
}

function executeOrReturnEmptyOnError(aOperation) {
  let result = [];

  try {
    result = aOperation();
  } catch(aException) {
  }

  return result;
}

function executeIfNeededOrReturnEmpty(aOperation, aCondition) {
  if (aCondition) {
    return aOperation();
  }

  return [];
}

module.exports = FindLocationsByNameController;

