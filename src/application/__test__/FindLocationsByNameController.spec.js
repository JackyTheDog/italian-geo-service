
const FindLocationsByNameController = require('../FindLocationsByNameController');
const CitiesJSONRepository = require('../../infrastructure/repositories/CitiesJSONRepository');
const ProvincesJSONRepository = require('../../infrastructure/repositories/ProvincesJSONRepository');
const RegionsJSONRepository = require('../../infrastructure/repositories/RegionsJSONRepository');
const LocationNameMatcher = require('../../infrastructure/matchers/LocationNameMatcher');

const City = require('../../domain/City');
const Province = require('../../domain/Province');
const Region = require('../../domain/Region');

describe('Find locations by name controller', () => {
  let controller;
  let aLocationNameMatcher;
  let aCitiesJSONRepo;
  let aProvincesJSONRepo;
  let aRegionsJSONRepo;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();

    aCitiesJSONRepo = new CitiesJSONRepository(aLocationNameMatcher);
    aProvincesJSONRepo = new ProvincesJSONRepository(aLocationNameMatcher);
    aRegionsJSONRepo = new RegionsJSONRepository(aLocationNameMatcher);

    controller = new FindLocationsByNameController(
      aCitiesJSONRepo,
      aProvincesJSONRepo,
      aRegionsJSONRepo
    );
  });

  describe('when required to find a location by matching name', () => {
    describe('and no location types list is provided', () => {
      it('should return matching cities, provinces and regions, if any', () => {
        let matchingLocations = controller
          .findByMatchingName('lo');

        expect(matchingLocations.length).toBe(61);
        expect(matchingLocations[0]).toBeInstanceOf(City);
        expect(matchingLocations[59]).toBeInstanceOf(Province);
        expect(matchingLocations[60]).toBeInstanceOf(Region);
      });
    });

    describe('and a location types list is provided', () => {
      describe('with only cities allowed', () => {
        it('should return matching cities, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['CITIES']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(City);
          });
        });
      });

      describe('with only provinces allowed', () => {
        it('should return matching provinces, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['PROVINCES']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(Province);
          });
        });
      });

      describe('with only regions allowed', () => {
        it('should return matching regions, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['REGIONS']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(Region);
          });
        });
      });
    });
  });
});
