
const FindLocationsByIdController = require('../FindLocationsByIdController');
const CitiesJSONRepository = require('../../infrastructure/repositories/CitiesJSONRepository');
const ProvincesJSONRepository = require('../../infrastructure/repositories/ProvincesJSONRepository');
const RegionsJSONRepository = require('../../infrastructure/repositories/RegionsJSONRepository');
const LocationNameMatcher = require('../../infrastructure/matchers/LocationNameMatcher');

describe('Find locations by id controller', () => {
  let controller;
  let aLocationNameMatcher;
  let aCitiesJSONRepo;
  let aProvincesJSONRepo;
  let aRegionsJSONRepo;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();

    aCitiesJSONRepo = new CitiesJSONRepository(aLocationNameMatcher);
    aProvincesJSONRepo = new ProvincesJSONRepository(aLocationNameMatcher);
    aRegionsJSONRepo = new RegionsJSONRepository(aLocationNameMatcher);

    controller = new FindLocationsByIdController(
      aCitiesJSONRepo,
      aProvincesJSONRepo,
      aRegionsJSONRepo
    );
  });

  describe('when required to find a city by IstatCode', () => {
    describe('and a city is found', () => {
      it('should return the city', () => {
        expect(
          controller
            .findCityByIstatCode('001001')
            .valueOf()
        )
          .toEqual({
            'istatCode': '001001',
            'name': 'Agliè',
            'provinceId': '001',
            'provinceCode': 'TO',
            'regionId': '01',
            'type': 'CITY'
          });
      });
    });

    describe('and a city is not found', () => {
      it('should return "null"', () => {
        expect(controller.findCityByIstatCode('BOH'))
          .toBe(null);
      });
    });
  });

  describe('when required to find a province by id', () => {
    describe('and a province is found', () => {
      it('should return the province', () => {
        expect(
          controller
            .findProvinceById('001')
            .valueOf()
        )
          .toEqual({
            'id': '001',
            'name': 'Torino',
            'provinceCode': 'TO',
            'regionId': '01',
            'type': 'PROVINCE'
          });
      });
    });

    describe('and a province is not found', () => {
      it('should return "null"', () => {
        expect(controller.findProvinceById('BOH'))
          .toBe(null);
      });
    });
  });

  describe('when required to find a region by id', () => {
    describe('and a region is found', () => {
      it('should return the region', () => {
        expect(
          controller
            .findRegionById('01')
            .valueOf()
        )
          .toEqual({
            'id': '01',
            'name': 'Piemonte',
            'regionCode': 'PIEMONTE',
            'type': 'REGION'
          });
      });
    });

    describe('and a region is not found', () => {
      it('should return "null"', () => {
        expect(controller.findRegionById('BOH'))
          .toBe(null);
      });
    });
  });
});
