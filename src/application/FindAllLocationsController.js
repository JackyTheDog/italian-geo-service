
const CityNotFoundException = require('../domain/exceptions/CityNotFoundException');

class FindLocationsByIdController {

  constructor(
    aCitiesJSONRepo,
    aProvincesJSONRepo,
    aRegionsJSONRepo
  ) {
    this.citiesJSONRepo = aCitiesJSONRepo;
    this.provincesJSONRepo = aProvincesJSONRepo;
    this.regionsJSONRepo = aRegionsJSONRepo;
  }

  findCityByIstatCode(aIstatCode) {
    return executeOrReturnNullOnError(
      this.citiesJSONRepo.findByIstatCode
        .bind(this.citiesJSONRepo, aIstatCode)
    );
  }

  findProvinceById(aProvinceId) {
    return executeOrReturnNullOnError(
      this.provincesJSONRepo.findById
        .bind(this.provincesJSONRepo, aProvinceId)
    );
  }

  findRegionById(aRegionId) {
    return executeOrReturnNullOnError(
      this.regionsJSONRepo.findById
        .bind(this.regionsJSONRepo, aRegionId)
    );
  }
}

function executeOrReturnNullOnError(aOperation) {
  let result = null;

  try {
    result = aOperation();
  } catch(aException) {
  }

  return result;
}

module.exports = FindLocationsByIdController;

