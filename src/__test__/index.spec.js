
const geoService = require('../index.js');

describe('The module', () => {
  describe('#getCityByIstatCode', () => {
    it('should delegate to the controller and work as expected', () => {
      expect(geoService.getCityByIstatCode('001001').istatCode())
        .toBe('001001');
    });
  });

  describe('#getProvinceById', () => {
    it('should delegate to the controller and work as expected', () => {
      expect(geoService.getProvinceById('001').id())
        .toBe('001');
    });
  });

  describe('#getRegionById', () => {
    it('should delegate to the controller and work as expected', () => {
      expect(geoService.getRegionById('01').id())
        .toBe('01');
    });
  });

  describe('#searchLocationsByName', () => {
    it('should delegate to the controller and work as expected', () => {
      expect(geoService.searchLocationsByName('Piemonte').length)
        .toBe(1);
    });
  });
});
