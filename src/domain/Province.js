
const ValueObject = require('value-objects').ValueObject;

class Province extends ValueObject {

  constructor({ id, name, provinceCode, regionId }) {
    super({ id, name, provinceCode, regionId, type: 'PROVINCE' });
  }
}

module.exports = Province;
