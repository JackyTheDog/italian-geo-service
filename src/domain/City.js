
const ValueObject = require('value-objects').ValueObject;

class City extends ValueObject {

  constructor({ istatCode, name, regionId, provinceId, provinceCode }) {
    super({ istatCode, name, regionId, provinceId, provinceCode, type: 'CITY' });
  }
}

module.exports = City;
