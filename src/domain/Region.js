
const ValueObject = require('value-objects').ValueObject;

class Region extends ValueObject {

  constructor({ id, name, regionCode }) {
    super({ id, name, regionCode, type: 'REGION' });
  }
}

module.exports = Region;
