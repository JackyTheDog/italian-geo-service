
const citiesJSON = require('../../../data/IT/cities.json');
const City = require('../City');

describe('City', () => {
  describe('when initialized', () => {
    it('should expose the city attributes as expected', () => {
      let cityJSON = citiesJSON[0];
      let city = new City(cityJSON);

      expect(city.name()).toBe('Agliè');
      expect(city.istatCode()).toBe('001001');
      expect(city.regionId()).toBe('01');
      expect(city.provinceId()).toBe('001');
      expect(city.type()).toBe('CITY');
    });
  });
});
