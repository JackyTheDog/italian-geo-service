
const LocationNameMatcher = require('./infrastructure/matchers/LocationNameMatcher');

const CitiesJSONRepository = require('./infrastructure/repositories/CitiesJSONRepository');
const ProvincesJSONRepository = require('./infrastructure/repositories/ProvincesJSONRepository');
const RegionsJSONRepository = require('./infrastructure/repositories/RegionsJSONRepository');

const FindLocationsByIdController = require('./application/FindLocationsByIdController');
const FindLocationsByNameController = require('./application/FindLocationsByNameController');

const locationNameMatcher = new LocationNameMatcher();

const citiesJSONRepository = new CitiesJSONRepository(locationNameMatcher);
const provincesJSONRepository = new ProvincesJSONRepository(locationNameMatcher);
const regionsJSONRepository = new RegionsJSONRepository(locationNameMatcher);

const findLocationsByIdController = new FindLocationsByIdController(
  citiesJSONRepository,
  provincesJSONRepository,
  regionsJSONRepository
);

const findLocationsByNameController = new FindLocationsByNameController(
  citiesJSONRepository,
  provincesJSONRepository,
  regionsJSONRepository
);

module.exports = {

  getCityByIstatCode(aIstatCode) {
    return findLocationsByIdController
      .findCityByIstatCode(aIstatCode);
  },

  getProvinceById(aProvinceId) {
    return findLocationsByIdController
      .findProvinceById(aProvinceId);
  },

  getRegionById(aRegionId) {
    return findLocationsByIdController
      .findRegionById(aRegionId);
  },

  searchLocationsByName(aNameNeedle, aListOfLocationsTypes) {
    return findLocationsByNameController
      .findByMatchingName(aNameNeedle, aListOfLocationsTypes);
  }

};

